import { renderFoodList,layThongTinTuForm } from "./controller.js";
import {Food} from "../../models/v2/model.js"
const BASE_URL = "https://64637f844dca1a66135fad18.mockapi.io/food"
var idSelect = null
let fetchFood= () =>{
    axios({
        url: BASE_URL,
        method: "GET"
        
    })
    .then((res)=>{console.log(res);
        let foodArr = res.data.map((item)=>{
        
        let {tenMon,loai,khuyenMai,hinhMon,moTa,giaMon,tinhTrang,maMon} = item
        let food = new Food(maMon,tenMon,loai,giaMon,khuyenMai,tinhTrang,hinhMon,moTa )
        return food 


        })
    renderFoodList(foodArr)
    })
    .catch((err)=>{console.log(err);})
}
fetchFood()
 
window.themMon = ()=>{
    let data = layThongTinTuForm()
    
    axios({
        url: BASE_URL,
        method: "POST",
        data: data,
    })
    .then((res)=>{
        $("#exampleModal").modal("hide")
        console.log(res)
        fetchFood();
    })
    .catch((err)=>{console.log(err);})
    
   }


 window.xoa=   (id)=>{
    axios({
        url: `${BASE_URL}/${id}`,
        method: "DELETE",
    })
    .then((res)=>{
        fetchFood()
    }

    )
    .catch((err)=>{
        console.log(err);

    })
 }
 window.sua= (id)=>{
    idSelect = id
    axios({
        url:`${BASE_URL}/${id}`,
        method: "GET"
    })
    .then((res)=>{
        $("#exampleModal").modal("show")
        
        let {maMon,loai,giaMon,hinhMon,tinhTrang,moTa,khuyenMai,tenMon}=res.data
        document.getElementById("foodID").value= maMon
        document.getElementById("tenMon").value= tenMon
        document.getElementById("loai").value= loai =  "loai1" ? "loai1":"loai2" 
        document.getElementById("giaMon").value=  giaMon
        document.getElementById("khuyenMai").value= khuyenMai == true?"10":"20"
        document.getElementById("tinhTrang").value= tinhTrang = 1 ? "1 ":"2"
        document.getElementById("hinhMon").value = hinhMon
        document.getElementById("moTa").value= moTa
    })
    .catch((err)=>{
        console.log(err);
    })


   }
   window.capNhatMon = ()=>{
     
    
    axios({
        url: `${BASE_URL}/${idSelect}`,
        method: "PUT",
        data: layThongTinTuForm(),
    })
    .then((res)=>{
        $("#exampleModal").modal("hide")
        console.log(res)
        fetchFood();
    })
    .catch((err)=>{console.log(err);})

   }
  