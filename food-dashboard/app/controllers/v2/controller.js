export function layThongTinTuForm(){
    let maMon = document.getElementById("foodID").value
    let tenMon = document.getElementById("tenMon").value
    let loai  = document.getElementById("loai").value
    let giaMon = document.getElementById("giaMon").value*1
    let khuyenMai = document.getElementById("khuyenMai").value*1
    let tinhTrang = document.getElementById("tinhTrang").value
    let hinhMon = document.getElementById("hinhMon").value
    let moTa = document.getElementById("moTa").value
    return {
        maMon,tenMon,loai,giaMon,khuyenMai,tinhTrang,hinhMon,moTa
    }
}

export let renderFoodList = (food)=>{
    let contentHTML = ""
    food.forEach((item)=>{
        let{maMon,tenMon,loai,giaMon,khuyenMai,tinhTrang} = item
        let content = `
        <tr> 
        <td>${maMon}</td>
        <td>${tenMon}</td>
        <td>${loai == true?"Chay":"Mặn" }</td>
        <td>${giaMon}</td>
        <td>${khuyenMai == true?"10":"20"}</td>
        <td>${item.giaKM()}</td>
        <td>${tinhTrang == true?"Còn":"Hết"}</td>
        <td><button class="btn btn-primary" onclick="sua(${maMon})">Sửa</button></td>
        <td><button class = "btn btn-danger" onclick="xoa(${maMon})"  >Xóa</button></td>

        </tr>
        `
        contentHTML += content
    })
    document.getElementById("tbodyFood").innerHTML = contentHTML
    


}